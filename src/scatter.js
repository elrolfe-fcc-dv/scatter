let dataset;

const displayData = () => {
  d3.select("#title")
    .text("Doping in Professional Bicycle Racing");

  d3.select("#subtitle")
    .text("35 Fastest Times up Alpe d'Huez");

  const margins = {
    left: 60,
    right: 20,
    top: 20,
    bottom: 60
  };
  const width = 1000 - margins.right - margins.left;
  const height = 750 - margins.top - margins.bottom;

  const xScale = d3.scaleLinear()
    .domain([
      d3.min(dataset, d => d.Year) - 1,
      d3.max(dataset, d => d.Year) + 1
    ])
    .range([margins.left, width + margins.left]);

  const yScale = d3.scaleTime()
    .domain([
      d3.min(dataset, d => new Date(0, 0, 0, 0, 0, d.Seconds)),
      d3.max(dataset, d => new Date(0, 0, 0, 0, 0, d.Seconds))
    ])
    .range([margins.top, height + margins.top]);

  const xAxis = d3.axisBottom(xScale);
  const yAxis = d3.axisLeft(yScale);
  const svg = d3
    .select("svg")
    .attr("width", width + margins.left + margins.right)
    .attr("height", height + margins.top + margins.bottom);

  svg
    .selectAll(".dot")
    .data(dataset)
    .enter()
    .append("circle")
    .attr("cx", d => xScale(d.Year))
    .attr("cy", d => yScale(new Date(0, 0, 0, 0, 0, d.Seconds)))
    .attr("r", 8)
    .attr("class", d => "dot " + (d.Doping.length === 0 ? "clean" : "doping"))
    .attr("data-xvalue", d => d.Year)
    .attr("data-yvalue", d => new Date(0, 0, 0, 0, 0, d.Seconds))
    .on("mouseover", d => {
      console.log(d3.event);
      d3.select("#name").text(d.Name);
      d3.select("#nationality").text(d.Nationality);
      d3.select("#year").text(d.Year);
      d3.select("#time").text(d.Time);
      d3.select("#description").text(d.Doping.length === 0 ? "No Doping Allegations" : d.Doping);
      d3.select("#tooltip")
        .attr("data-year", d.Year)
        .style("opacity", 1)
        .style("top", (d3.event.pageY - 20) + "px")
        .style("left", (d3.event.pageX + 35) + "px");
    })
    .on("mouseout", d => {
      console.log(d3.event);
      d3.select("#tooltip")
        .style("opacity", 0);
    });

  svg.append("g")
    .attr("id", "x-axis")
    .attr("transform", "translate(0, " + (height + margins.top) + ")")
    .call(xAxis
      .tickFormat(d3.format("4d"))
    );

  svg.append("g")
    .attr("id", "y-axis")
    .attr("transform", `translate(${margins.left}, 0)`)
    .call(yAxis
      .tickFormat(d3.timeFormat("%M:%S"))
    );

  const legend = svg.append("g")
    .attr("id", "legend");

  legend.append("rect")
    .attr("width", 180)
    .attr("height", 50)
    .attr("x", 800)
    .attr("y", 100)

  legend.append("circle")
    .attr("class", "doping")
    .attr("cx", 815)
    .attr("cy", 115)
    .attr("r", 5);

  legend.append("text")
    .attr("x", 830)
    .attr("y", 120)
    .text("Doping Allegations Made");

  legend.append("circle")
    .attr("class", "clean")
    .attr("cx", 815)
    .attr("cy", 135)
    .attr("r", 5);

  legend.append("text")
    .attr("x", 830)
    .attr("y", 140)
    .text("No Doping Allegations");
};

const loadData = () => {
  const req = new XMLHttpRequest();
  req.open("GET", "https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/cyclist-data.json", true);
  req.onload = () => {
    dataset = JSON.parse(req.responseText);
    displayData();
  };
  req.send();
};

document.addEventListener("DOMContentLoaded", loadData);
